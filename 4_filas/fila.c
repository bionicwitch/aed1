#include "fila.h"

struct fila * create(){
  struct fila *um;
  struct llist *lista;
  um=malloc(sizeof (struct fila));
  lista=create_l();
  if(um==NULL || lista==NULL){
    return NULL;
  }
  else
  {
    um->ponte=lista;
    return um;    
  }
  return NULL; 

};


int makenull(struct fila * f){
  if(f->ponte->tam==0){
    return 1; //listavazia;
  }
  else
  {
    struct elem *aux;
    for (int i = 0; i < f->ponte->tam; i++)
    {
      aux=f->ponte->cabeca;
      f->ponte->cabeca->next=aux->next;
      free(aux);
    }
    f->ponte->tam=0;
    return 1;
    
  }  
  return 0;
};


int dequeue(struct fila * f){
  if (f->ponte->tam==0)
  {
    return 0;
  }
  else
  {
    int k=f->ponte->cabeca->val;
    struct elem *pont;
    pont=f->ponte->cabeca->next;
    free(f->ponte->cabeca);
    f->ponte->cabeca=pont;
    f->ponte->tam--;
    return k;
  }
  return 0;
  

};

int enqueue(struct fila * f, int val){
  struct elem *xis;
  xis=create_node(val);
  if (f->ponte->cabeca==NULL)
  {
    f->ponte->cabeca=xis;
    f->ponte->cabeca->next = NULL;
    insert_l(f->ponte,NULL,f->ponte->cabeca);
    f->last = f->ponte->cabeca;
    return 1;
    }
  else
   {
    f->last->next = NULL;
    insert_l(f->ponte,f->last,xis);
    f->last=xis;
    return 1;
   }

  return 0;
}


 int vazia(struct fila * f){
  if (f->ponte->tam==0)
   {
     return 1;
   }
  else
   {
      return 0;
   }
   return 0;
   
   
 };


void destroy(struct fila * f){
  destroy_l(f->ponte);
  free(f);
};
