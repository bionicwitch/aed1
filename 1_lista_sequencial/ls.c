#include "ls.h"
#include <stdio.h>
#include <stdlib.h>

struct list * create(int max){
    if (max<=0)
          return NULL;
    else
    {   
        struct list *pont;
	    pont = malloc(sizeof (struct list));	
    	pont->arm=malloc(max * sizeof (elem));
    	pont->capacidade=max;
        pont->ultimo=0;
        return pont;
    }
};

int insert(struct list *desc, int pos, elem item){
    if(pos-1 >= desc->capacidade || pos-1 <= -1)
        return 0; //pos invalida
    else
    {
        for(int i = desc->ultimo ; i < pos-1 ; i--)
        {
            desc->arm[i+1]=desc->arm[i];
        }
        desc->ultimo++;
        desc->arm[pos-1]=item;
        return 1;//moveu os itens e alocou o novo
    }
};


int removel(struct list *desc, int pos)
{
	if((pos-1)>desc->capacidade || (pos-1)<0){
     //   printf("pos invalida");
        return 0;
    }
    else {
        if (pos-1 == desc->ultimo) {
            desc->arm[pos-1]=0;
            desc->ultimo--;
            return 1;
        }
        else
        {
            for(int i = pos-1 ; i < desc->ultimo ; i++)
            {
                desc->arm[i]=desc->arm[i+1];
            }
            desc->arm[desc->ultimo]=0;
            desc->ultimo--;
            return 1;
        }        
    }	
};


elem get(struct list *desc, int pos){

    if((pos-1)>desc->capacidade || (pos-1)<0){
       // printf("pos invalida");
        return 0;
    }
    else {
        return desc->arm[pos-1];
    }
    return 0;
};

int set(struct list *desc, int pos, elem item){
	if(pos-1 > desc->capacidade || pos-1 > 0 )
        return 0;
    else {
		desc->arm[pos-1]=item;
        return 1;
        }
     //return 0;
};

int locate(struct list *desc, int pos, elem item){
	int i;
	for(i=pos-1;i < desc->ultimo ;i++){
			if(desc->arm[i]==item){
				return i+1;
			}
		}
	return 0;
};


int length(struct list *desc){
	return desc->ultimo;
};

int max(struct list *desc){
	return desc->capacidade;
	};


int full(struct list *desc){
	if(length(desc)==max(desc))
		return 1;
	else
		return 0;
	};


void destroy(struct list *desc){
	free(desc);
	};
